package com.alecbrando.homeworkrecyclerview.util

import com.alecbrando.homeworkrecyclerview.model.models.Data

sealed class ResourceWrapper(data: List<Data>?){
    data class Success(val data: List<Data>): ResourceWrapper(data)
    object Loading: ResourceWrapper(null)



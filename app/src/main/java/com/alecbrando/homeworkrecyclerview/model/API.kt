package com.alecbrando.homeworkrecyclerview.model

import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.models.Data

interface API {
    suspend fun fetchAnimes(data: Animes): List<Data>
}
package com.alecbrando.homeworkrecyclerview.model.models

data class TinyX(
    val height: Int,
    val width: Int
)
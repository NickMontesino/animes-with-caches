package com.alecbrando.homeworkrecyclerview.model.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Data(
    val attributes: Attributes,
    val id: String? = "",
    val type: String? = ""
): Parcelable